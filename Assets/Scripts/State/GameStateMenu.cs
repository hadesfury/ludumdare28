﻿using UnityEngine;
using System.Collections;

public class GameStateMenu : State<GameManager,string>
{
    public override void Enter ( GameManager owner )
	{

	}
    public override void Execute ( GameManager owner, float time_step )
    {
        if ( ( Application.loadedLevelName != "StartScreen" ) || ( Input.GetMouseButtonUp( 0 ) ) )
        {
            owner.ChangeGameState( typeof( GameStatePlay ) );
        }
	}
    public override void Exit ( GameManager owner )
	{      
        if ( Application.loadedLevelName == "StartScreen" )
        {            
            owner.PlayIntroCutScene();
            Application.LoadLevel( "ShackExterior" );
        }
	}
    public override bool OnMessage ( GameManager owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
	}
}
