﻿using UnityEngine;
using System.Collections;

public class GameStatePlay : State<GameManager,string>
{
    public override void Enter ( GameManager owner )
	{

	}
    public override void Execute ( GameManager owner, float time_step )
    {
        foreach ( PickableObject current_object in owner.GetInventoryManager().GetInventoryTable() )
        {
            WetablePickableObject 
                wetable_object = current_object.GetComponent<WetablePickableObject>();
            
            if( wetable_object != null )
            {
                if( wetable_object.isWet )
                {
                    owner.ChangeGameState( typeof( GameStateGameOver ) );
                }
            }
        }
	}
    public override void Exit ( GameManager owner )
	{

	}
    public override bool OnMessage ( GameManager owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
  	}
}
