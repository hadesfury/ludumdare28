﻿using UnityEngine;
using System.Collections;

public class GameStateWinOver : State<GameManager,string>
{
    public override void Enter ( GameManager owner )
	{

        Application.LoadLevel( "WinOverScreen" );

	}
    public override void Execute ( GameManager owner, float time_step )
    {
        if ( Input.GetMouseButtonUp( 0 ) )
        {
            Application.ExternalEval( "document.location.reload(true)" );
        }

	}
    public override void Exit ( GameManager owner )
	{

	}
    public override bool OnMessage ( GameManager owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
	}
}
