
public class Message<_message_type_>
{
	
	public Message()
    {

    }

    public Message( _message_type_ message_type )
    {
		mType = message_type;
    }

    // ~~

    Message( Message<_message_type_> other )
    {
        mDelay = other.mDelay;
        mDispachTime = other.mDispachTime;
        mSenderId = other.mSenderId;
        mReceiverId = other.mReceiverId;
        mType = other.mType;
//        mExtraInfo = other.mExtraInfo;
    }

    // .. ATTRIBUTES

    public double
        mDelay = 0.0,
        mDispachTime = 0.0;
    public int
        mSenderId = -1,
        mReceiverId = -1;
    public _message_type_
        mType;
//    var
//        mExtraInfo;	
	
};
