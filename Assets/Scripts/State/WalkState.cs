﻿using UnityEngine;
using System.Collections;

public class WalkState : State<PlayerController,string>
{
	public override void Enter ( PlayerController owner )
	{
		owner.GetComponentInChildren<Animation>().CrossFade( "walk", 0.2f );
		owner.audio.clip = owner.footStepClip;
		owner.audio.Play();
	}
	public override void Execute ( PlayerController owner, float time_step )
	{
		NavMeshAgent
			nav_mesh_agent = owner.GetComponent<NavMeshAgent>();
		
		if( !owner.audio.isPlaying )
		{
			owner.audio.Play();
		}

		if( !nav_mesh_agent.hasPath )
		{
			owner.ChangeState( typeof( IdleState ) );
		}
	}
	public override void Exit ( PlayerController owner )
	{
		owner.audio.Stop();
	}
	public override bool OnMessage ( PlayerController owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
	}
}
