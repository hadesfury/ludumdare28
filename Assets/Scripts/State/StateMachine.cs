using UnityEngine;
using System.Collections.Generic;
using System;

public class StateMachine<_entity_type_, _message_type_>
{
	// .. CONSTRUCTORS

	public StateMachine( _entity_type_ owner )
    {
		mOwner = owner;
    }

    // .. ACCESSORS

    public void SetCurrentState( Type state_type )
    {
        mCurrentState = mStateTable[ state_type ];

        mCurrentState.Enter( mOwner );
    }

    // ~~

    private void SetCurrentState( State<_entity_type_, _message_type_> current_state ) 
    { 
        mCurrentState = current_state; 
        
        mCurrentState.Enter( mOwner );
    }

    // ~~

    public void SetGlobalState( Type state_type )
    {
        mGlobalState = mStateTable[ state_type ];
    }

    // ~~

    private void SetGlobalState( State<_entity_type_, _message_type_> global_state )
    { 
        mGlobalState = global_state;
    }

    // ~~

    public void SetPreviousState( Type state_type )
    {
        mPreviousState = mStateTable[ state_type ];
    }

    // ~~

    private void SetPreviousState( State<_entity_type_, _message_type_> previous_state ) 
    { 
        mPreviousState = previous_state;
    }

    // ~~

    public State<_entity_type_, _message_type_> GetState( Type state_type )
    {
        return mStateTable[ state_type ];
    }

    // ~~

    public Type GetCurrentState()
    {
        Type
            return_value = null;//( Type ) ( object ) ( -1 );

        foreach ( KeyValuePair<Type, State<_entity_type_, _message_type_>> state_pair in mStateTable )
        {
            if ( state_pair.Value == mCurrentState  )
            {
                return_value = state_pair.Key;
            }
        }

        return return_value;
    }

    // ~~

    public Type GetPreviousState()
    {
        Type
            return_value = null;//( Type ) ( object ) ( -1 );

        foreach ( KeyValuePair<Type, State<_entity_type_, _message_type_>> state_pair in mStateTable )
        {
            if ( state_pair.Value == mPreviousState )
            {
                return_value = state_pair.Key;
            }
        }

        return return_value;
    }

    // ~~

    State<_entity_type_, _message_type_> GetGlobalState()
    { 
        return mGlobalState; 
    }

    // .. OPERATIONS

    public void RegisterState( State<_entity_type_, _message_type_> new_state )
    {
		Type
			state_type = new_state.GetType();
		
        if ( !mStateTable.ContainsKey( state_type ) )
        {
            mStateTable.Add( state_type, new_state );
        }
    }
    
    public void ChangeState( Type state_type )
    {
        if ( !mStateTable.ContainsKey( state_type ) )
        {
            Debug.LogError( "<StateMachine::ChangeState>: trying to change to a non-registered state" );
        }

        mPreviousState = mCurrentState;

        mCurrentState.Exit( mOwner );

        mCurrentState = mStateTable[ state_type ];

        mCurrentState.Enter( mOwner );
    }

    private void ChangeState( State<_entity_type_, _message_type_> new_state )
    {
		if ( new_state == null )
		{
			Debug.LogError( "<StateMachine::ChangeState>: trying to change to a null state" );
		}

        mPreviousState = mCurrentState;

        mCurrentState.Exit( mOwner );

        mCurrentState = new_state;

        mCurrentState.Enter( mOwner );
    }

    // ~~

    void RevertToPreviousState()
    {
		if ( mCurrentState == mPreviousState )
		{
			Debug.LogError( "mCurrentState == mPreviousState" );
		}

        ChangeState( mPreviousState );
    }

    // ~~

    public bool HandleMessage( Message<_message_type_> message )
    {
        if ( ( mCurrentState != null ) && mCurrentState.OnMessage( mOwner, message ) )
        {
            return true;
        }

        if ( ( mGlobalState != null ) && mGlobalState.OnMessage( mOwner, message ) )
        {
            return true;
        }

        return false;
    }

    // ~~

    public void Update( float time_step )
    {
        if ( mGlobalState != null )
        {
            mGlobalState.Execute( mOwner, time_step );
        }

        if ( mCurrentState != null )
        {
            mCurrentState.Execute( mOwner, time_step );
        }
    }

	private StateMachine(){}
    private StateMachine( StateMachine<_entity_type_, _message_type_> other ) { }

	// .. ATTRIBUTES

    State<_entity_type_, _message_type_>
        mCurrentState,
        mPreviousState,
        mGlobalState;
    _entity_type_ 
        mOwner;
    Dictionary<Type, State<_entity_type_, _message_type_>>
        mStateTable = new Dictionary<Type,State<_entity_type_,_message_type_>>();
};

