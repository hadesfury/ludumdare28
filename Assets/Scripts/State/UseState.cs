﻿using UnityEngine;
using System.Collections;

public class UseState : State<PlayerController,string>
{
	public override void Enter ( PlayerController owner )
	{
		owner.GetComponentInChildren<Animation>().CrossFade( "use", 0.2f );
		owner.audio.clip = owner.useClip;
		owner.audio.Play();
	}

	public override void Execute ( PlayerController owner, float time_step )
	{
        if( !owner.GetComponentInChildren<Animation>().isPlaying )
        {
            owner.ChangeState( typeof( IdleState ) );
        }
	}

	public override void Exit ( PlayerController owner )
	{
		//owner.GetComponentInChildren<Animation>().Stop();
	}

	public override bool OnMessage ( PlayerController owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
	}
}
