

abstract public class State<_entity_type_, _message_type_>
{
    // .. OPERATIONS

    public abstract void Enter( _entity_type_ owner );
    public abstract void Execute( _entity_type_ owner, float time_step );
    public abstract void Exit( _entity_type_ owner );
    public abstract bool OnMessage( _entity_type_ owner, Message<_message_type_> message );

	protected State()
	{
	}
		
	// .. CONSTRUCTORS
	private State( State<_entity_type_,_message_type_> other )
	{
	}

	// .. OPERATORS

	
};