﻿using UnityEngine;
using System.Collections;

public class IdleState : State<PlayerController,string>
{
	public override void Enter ( PlayerController owner )
	{
		owner.GetComponentInChildren<Animation>().CrossFade( "idle", 0.2f );
	}
	public override void Execute ( PlayerController owner, float time_step )
	{
		//owner.GetComponentInChildren<Animation>().Play( "idle" );
	}
	public override void Exit ( PlayerController owner )
	{
		//owner.GetComponentInChildren<Animation>().Stop();
	}
	public override bool OnMessage ( PlayerController owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
	}
}
