using UnityEngine;
using System.Collections;

public class GameStateGameOver : State<GameManager,string>
{
    public override void Enter ( GameManager owner )
	{
        bool
            has_found_wet_matches = false,
            has_found_wet_log = false;

        foreach ( PickableObject current_object in owner.GetInventoryManager().GetInventoryTable() )
        {
            WetablePickableObject 
                wetable_object = current_object.GetComponent<WetablePickableObject>();
            
            if( wetable_object != null )
            {
                if( wetable_object.isWet )
                {
                    if( current_object.name == "CompanionLog" )
                    {
                        has_found_wet_log = true;
                    }

                    if( current_object.name == "MatchesBox" )
                    {
                        has_found_wet_matches = true;

                        break;
                    }
                }
            }
        }

        if( has_found_wet_matches )
        {
            owner.PlayGameOverWetMatchesCutScene();
            Application.LoadLevel( "GameOverScreen" );
        }
        else if( has_found_wet_log )
        {
            owner.PlayGameOverWetLogCutScene();
            Application.LoadLevel( "GameOverScreen" );
        }
        else
        {
            owner.PlayGameOverCutScene();
            Application.LoadLevel( "WinOverScreen" );
        }


	}
    public override void Execute ( GameManager owner, float time_step )
    {
        if ( Input.GetMouseButtonUp( 0 ) )
        {
            Application.ExternalEval( "document.location.reload(true)" );
        }

	}
    public override void Exit ( GameManager owner )
	{

	}
    public override bool OnMessage ( GameManager owner, Message<string> message )
	{
		throw new System.NotImplementedException ();
	}
}
