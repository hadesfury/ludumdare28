﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class PersistentPlayer
{
    public Dictionary<string,PersistentData>
        inventoryDataTable = new Dictionary<string, PersistentData>();
    public float[]
        position,
        orientation;

    public IEnumerator Load( PlayerController player )
    {
        if ( ( position != null ) && ( orientation != null ) )
        {
            Vector3
                player_position = new Vector3( position[ 0 ], position[ 1 ], position[ 2 ] );
            Quaternion
                player_orientation = new Quaternion( orientation[ 0 ], orientation[ 1 ], orientation[ 2 ], orientation[ 3 ] );

            player.transform.position = player_position;
            player.transform.rotation = player_orientation;

        }

        yield return player.StartCoroutine( LoadInventory( player.GetGameManager().GetPersistenceManager(), player.GetGameManager().GetInventoryManager() ) );
    }

    public void Save( PlayerController player )
    {

        Vector3
            player_position = player.transform.position;
        Quaternion
            player_orientation = player.transform.rotation;

        position = new float[] { player_position.x, player_position.y, player_position.z };
        orientation = new float[] { player_orientation.x, player_orientation.y, player_orientation.z, player_orientation.w };
    }

    private IEnumerator LoadInventory( PersistenceManager persistence_manager, InventoryManager inventory_manager )
    {
        foreach( PersistentData current_inventory_item_data in inventoryDataTable.Values )
        {
            PersistentObject
                persistent_object = persistence_manager.LoadPersistentObject( current_inventory_item_data );
            PickableObject
                pickable_object = persistent_object.GetComponent<PickableObject>();            
            
            if ( pickable_object != null )
            {
                inventory_manager.AddToInventory( pickable_object );
            }
            else
            {
                Debug.LogError( "Item saved in inventory is not pickable !!! " );
            }
        }

        yield return null;
    }
}
