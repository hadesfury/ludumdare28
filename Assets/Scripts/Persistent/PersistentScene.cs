using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PersistentScene
{
    public Dictionary<string,PersistentData>
        sceneDataTable = new Dictionary<string, PersistentData>();
}

