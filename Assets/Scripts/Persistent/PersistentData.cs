using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PersistentData
{
    public string
        name = "Default",
        prefabName = "";
    public bool
        isActive = true;
    public float[]
        position,
        orientation;
    public Dictionary<string,bool> // public for serialisation purpose
        mBooleanDataTable = new Dictionary<string, bool>();
    public Dictionary<string,string> // public for serialisation purpose
        mStringDataTable = new Dictionary<string, string>();

    public bool GetBoolData ( string key, out bool result )
    { 
        if( mBooleanDataTable.ContainsKey( key ) )
        {
            result = mBooleanDataTable[ key ];

            return true;
        }

        result = false;

        return false;
    }

    public void SetBoolData ( string key, bool value )
    {
        if( mBooleanDataTable.ContainsKey( key ) )
        {
            mBooleanDataTable.Remove( key );
        }

        mBooleanDataTable.Add( key, value );
    }
    
    public bool GetStringData ( string key, out string result )
    { 
        result = null;

        if( mStringDataTable.ContainsKey( key ) )
        {
            result = mStringDataTable[ key ];
            
            return true;
        }
        
        return false;
    }
    
    public void SetStringData ( string key, string value )
    {
        if( mStringDataTable.ContainsKey( key ) )
        {
            mStringDataTable.Remove( key );
        }
        
        mStringDataTable.Add( key, value );
    }
}

