using UnityEngine;
using System.Collections;

public class PersistentObject : ManagedComponent
{
    public string
        objectName;
    public GameObject
        prefab;

    void Start ()
    {
        mPersistenceManager = GetGameManager().GetPersistenceManager();
        
        mPersistenceManager.Register( this );
        mPersistenceManager.AddPersistentObjectToScene( this, Application.loadedLevelName );
    }
    
    void OnDestroy()
    {
        mPersistenceManager.Unregister( this );
    }

    public PersistentData GetPersistentData()
    {
        return mPersistentData;
    }

    public virtual void Load( PersistentData persistent_data )
    {
        Vector3
            position = new Vector3( persistent_data.position[ 0 ], persistent_data.position[ 1 ], persistent_data.position[ 2 ] );
        Quaternion
            orientation =  new Quaternion( persistent_data.orientation[ 0 ], persistent_data.orientation[ 1 ], persistent_data.orientation[ 2 ], persistent_data.orientation[ 3 ] );        
        Interaction
            interaction = GetComponent<Interaction>();
        
        mPersistentData = persistent_data;

        objectName = mPersistentData.name;

        gameObject.transform.position = position;
        gameObject.transform.rotation = orientation;

        gameObject.SetActive( mPersistentData.isActive );
        
        if( interaction != null )
        {
            interaction.Load( mPersistentData );
        }
    }

    public virtual void Save()
    {        
        Vector3
            position = gameObject.transform.position;
        Quaternion
            orientation = gameObject.transform.rotation;
        Interaction
            interaction = GetComponent<Interaction>();

        mPersistentData.name = objectName;

        if( prefab != null )
        {
            mPersistentData.prefabName = prefab.name;
        }
        
        mPersistentData.position = new float[] { position.x, position.y, position.z };
        mPersistentData.orientation = new float[] { orientation.x, orientation.y, orientation.z, orientation.w };   

        mPersistentData.isActive = gameObject.activeSelf;

        if( interaction != null )
        {
            interaction.Save( mPersistentData );
        }
    }
    
    protected PersistentData
        mPersistentData = new PersistentData();
    private PersistenceManager
        mPersistenceManager;
}

