using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PersistentGeneral
{
    public string
        currentSceneName;
    public Dictionary<string, PersistentScene>
        sceneTable = new Dictionary<string, PersistentScene>();
    public PersistentPlayer
        player = new PersistentPlayer();

}

