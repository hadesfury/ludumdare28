﻿using UnityEngine;
using System.Collections;

public class InventorySlot : ManagedComponent
{
    public delegate void VoidFunctionVoid( ); 
    public delegate void VoidFunctionPickableObject( PickableObject pickable_object ); 
    
    public void RegisterClickEvent( VoidFunctionVoid click_event_function )
    {
        mClickEventFunction = click_event_function;
    }

    public void RegisterClickPickableEvent( VoidFunctionPickableObject click_pickable_event_function, PickableObject pickable_object )
    {
        mClickEventPickableFunction = click_pickable_event_function;
        mPickableObject = pickable_object;
    }
    public void OnClick( dfControl control, dfMouseEventArgs mouseEvent )
    {
        if( mClickEventFunction != null )
        {
            mClickEventFunction();
        }
        else if ( ( mClickEventPickableFunction != null ) && ( mPickableObject != null ) )
        {
            mClickEventPickableFunction( mPickableObject );
        }
        
        GetGameManager().GetInputManager().hasInputBeenUsed = true;
    }
    
    private VoidFunctionVoid
        mClickEventFunction;
    private VoidFunctionPickableObject
        mClickEventPickableFunction;
    private PickableObject
        mPickableObject;
}
