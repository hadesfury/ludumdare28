﻿using UnityEngine;
using System.Collections;

public class LakePortalInteraction : WetPortalObject
{
    public bool
        isLocked;

    public override void Use()
    {   
        if( !isLocked )
        {
            base.Use();
        }
        else
        {
            PlayerController
                player = GetGameManager().GetPlayer();            
            
            player.transform.rotation = anchorPoint.rotation;
            player.GetComponent<CharacterTextController>().Speak( useText );

        }
    }
    
    public override void Load( PersistentData persistent_data )
    {
        base.Load( persistent_data );
        
        persistent_data.GetBoolData( "isLocked", out isLocked );
    }

    public override void Save ( PersistentData persistent_data )
    {
        base.Save ( persistent_data );

        persistent_data.SetBoolData( "isLocked", isLocked );
    }
}
