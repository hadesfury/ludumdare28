using UnityEngine;
using System.Collections;

public class ChimneyInteraction : Interaction
{
    public string
        logThownText = "I hope it will work",
        tunnelDestinationScene = "ShackInterior";

    public override void Use ()
    {
        PlayerController
            player = GetGameManager().GetPlayer();

        if( player.handledPickableObject == null )
        {
            base.Use();
        }
        else if( player.handledPickableObject.name == "CompanionLog" )
        {
            PickableObject
                log = player.handledPickableObject;
            PersistenceManager
                persistence_manager = GetGameManager().GetPersistenceManager();
                
            log.gameObject.SetActive( true );
            
            player.transform.rotation = anchorPoint.rotation;
            player.ConsumeObject( true );
            // HACK we don't really destroy it
            persistence_manager.AddPersistentObjectToScene( log.GetComponent<PersistentObject>(), tunnelDestinationScene );
            log.transform.localPosition = Vector3.zero;
            log.transform.localRotation = Quaternion.identity;
                
            player.GetComponent<CharacterTextController>().Speak( logThownText );
            
            log.gameObject.SetActive( false );
            //GameObject.Destroy( log.gameObject );
        }
    }
}

