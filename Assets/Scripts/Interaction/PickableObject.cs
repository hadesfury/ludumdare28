﻿using UnityEngine;
using System.Collections;

public class PickableObject : Interaction
{
	public Texture2D
		inventoryIcon;
    public bool
        isPickable = true;
    public string
        inventoryIconSpriteName,
        objectIsNotPickableMessage = "Not reachable";

    public delegate void VoidFunctionVoid();

    public void RegisterOnPickedFunction( VoidFunctionVoid on_picked_function )
    {
        mOnPickedFunction = on_picked_function;
    }

    public override void Use()
    {
        PlayerController
            player = GetGameManager().GetPlayer();
        
        if ( ( player.handledPickableObject == null ) && isPickable )
        {
            player.transform.rotation = anchorPoint.rotation;

            GetGameManager().GetInventoryManager().AddToInventory( this );

            if( mOnPickedFunction != null )
            {
                mOnPickedFunction();
            }
        }
        else if ( !isPickable )
        {  
            player.GetComponent<CharacterTextController>().Speak( objectIsNotPickableMessage );
        }
        else
        {
            base.Use();
        }
    }

    private VoidFunctionVoid
        mOnPickedFunction;

}
