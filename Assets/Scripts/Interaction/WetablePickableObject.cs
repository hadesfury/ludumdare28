﻿using UnityEngine;
using System.Collections;

public class WetablePickableObject : PickableObject
{
    public bool
        isWet = false;
    public string
        wetObjectMessage = "It's wet ! So unusable :( ";

    public override void Use()
       {
        PlayerController
            player = GetGameManager().GetPlayer();

        if( isWet )
        {
            player.GetComponent<CharacterTextController>().Speak( wetObjectMessage );
        }
        else
        {
            base.Use();
        }
    }

    private VoidFunctionVoid
        mOnPickedFunction;

}
