using UnityEngine;
using System.Collections;

public class TreeBranchInteraction : Interaction
{
    public TreeInteraction
        treeInteraction;
    public Transform
        upAnchorTransform,
        downAnchorTransform;
    public bool
        doesClimbUp = true;

    public override void Use ()
    {
        PlayerController
            player = GetGameManager().GetPlayer();

        if( player.handledPickableObject == null )
        {
            if( doesClimbUp )
            {
                if( treeInteraction.GetIsClimblable() )
                {

                    player.GetComponent<NavMeshAgent>().enabled = false;
                    player.transform.position = upAnchorTransform.position;
                    player.SetCurrentTarget( null );
                    player.GetComponent<NavMeshAgent>().enabled = true;
                }
                else
                {
                    base.Use();
                }
            }
            else
            {
                player.GetComponent<NavMeshAgent>().enabled = false;
                player.transform.position = downAnchorTransform.position;
                player.SetCurrentTarget( null );
                player.GetComponent<NavMeshAgent>().enabled = true;
            }
        }
        else
        {
            base.Use();
        }
    }
}

