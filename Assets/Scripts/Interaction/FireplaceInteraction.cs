using UnityEngine;
using System.Collections;

public class FireplaceInteraction : Interaction
{
    public Transform
        logTransform;
    public GameObject
        fireObject;
    public float
        timeBeforeWin = 7;

    public override void Use ()
    {
        PlayerController
            player = GetGameManager().GetPlayer();

        if( mIsLogInPlace )
        {
            if ( ( player.handledPickableObject != null ) && ( player.handledPickableObject.name == "MatchesBox" ) )
            {
                mLogObject.collider.enabled = false;
                
                mLogObject.gameObject.SetActive( true );
                
                player.transform.rotation = anchorPoint.rotation;
                player.ConsumeObject( true );
                
                fireObject.SetActive( true );

                StartCoroutine( EndGameTimer() );
            }
        }
        else
        {

            if( player.handledPickableObject == null )
            {
                base.Use();
            }
            else if( player.handledPickableObject.name == "CompanionLog" )
            {
                mLogObject = player.handledPickableObject;

                mLogObject.gameObject.transform.position = logTransform.position;
                mLogObject.gameObject.transform.rotation = logTransform.rotation;
                
                mLogObject.gameObject.SetActive( true );

                player.handledPickableObject.RegisterOnPickedFunction( OnLogPicked );
                
                player.transform.rotation = anchorPoint.rotation;
                player.ConsumeObject( false );
                
                mIsLogInPlace = true;

            }
        }

    }

    private IEnumerator EndGameTimer()
    {

        yield return new WaitForSeconds( timeBeforeWin );

        GetGameManager().ChangeGameState( typeof( GameStateGameOver ) );
    }


    private void OnLogPicked()
    {
        mIsLogInPlace = false;
    }

    private PickableObject
        mLogObject;
    private bool
        mIsLogInPlace = false;
}

