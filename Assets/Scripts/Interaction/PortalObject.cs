﻿using UnityEngine;
using System.Collections;

public class PortalObject : Interaction
{
    public string
        sceneName;

    public override void Use()
    {
        PlayerController
            player_controller = GetGameManager().GetPlayer();
        
        if ( player_controller.handledPickableObject == null )
        {
            if ( ( sceneName != null ) && ( sceneName.Length > 0 ) )
            {
                GetGameManager().ChangeScene( sceneName );
            }
        }
        else
        {
            base.Use();
        }
    }
}
