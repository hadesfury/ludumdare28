﻿using UnityEngine;
using System.Collections;

public class WetPortalObject : PortalObject
{
    public override void Use()
    {   
        PlayerController
            player_controller = GetGameManager().GetPlayer();

        if ( player_controller.handledPickableObject == null )
        {
            InventoryManager
                inventory_manager = GetGameManager().GetInventoryManager();

            foreach ( PickableObject current_object in inventory_manager.GetInventoryTable() )
            {
                WetablePickableObject 
                    wetable_object = current_object.GetComponent<WetablePickableObject>();

                if( wetable_object != null )
                {
                    wetable_object.isWet = true;
                }
            }
            
            base.Use();
        }
        else
        {
            base.Use();
        }
    }
}
