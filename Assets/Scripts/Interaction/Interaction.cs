using UnityEngine;
using System.Collections;

public class Interaction : ManagedComponent
{   
    public string 
        cursorText = "Made in China",
        lookText = "It's probably made in China",
        useText = "I'm using it";
    public Transform
        anchorPoint;

    public void DisplayCursorText()
    {
        GetGameManager().GetGuiManager().GetCursor().SetText( cursorText );
    }
    
    public void Look()
    {
        PlayerController
            player = GetGameManager().GetPlayer();
        Vector3
            look_at_position = transform.position;

        player.GetComponent<NavMeshAgent>().enabled = false;

        look_at_position.y = player.transform.position.y;;

        player.transform.LookAt( look_at_position );
        
        player.GetComponent<CharacterTextController>().Speak( lookText );
        player.GetComponent<NavMeshAgent>().enabled = true;
    }

    public virtual void Use()
    {
        PlayerController
            player = GetGameManager().GetPlayer();
        
        player.GetComponent<CharacterTextController>().Speak( useText );

        player.transform.rotation = anchorPoint.rotation;

        player.ChangeState( typeof( UseState ) );
    }

    public virtual void Load( PersistentData persistent_data )
    {

    }
    
    public virtual void Save( PersistentData persistent_data )
    {
        
    }
}

