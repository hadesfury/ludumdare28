using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EdgeInteraction : Interaction
{   
    public List<Texture2D>
        sceneTable;
    public float
        sceneImageDiplayDuration = 5;
    public LakePortalInteraction
        lakePortal;
    public GameObject
        crackMesh,
        holeMesh;
    public string
        holeLookAtMessage,
        holeUseMessage;

    public override void Use()
    {
        StartCoroutine( PlayCutScene() );

//        GameManager 
//            game_manager = GameObject.Find( "GameManager" ).GetComponent<GameManager>();
//        PlayerController
//            player = game_manager.GetPlayer();
//        
//        player.GetComponent<CharacterTextController>().Speak( useText );
//
//        player.transform.rotation = anchorPoint.rotation;
//
//        player.ChangeState( typeof( UseState ) );
    }

    private IEnumerator PlayCutScene()
    {
        CutScenePlayer 
            cut_scene_player = GetGameManager().GetCutScenePlayer();

        yield return StartCoroutine( cut_scene_player.PlayCutSceneCoroutine( sceneTable, sceneImageDiplayDuration ) );

        lakePortal.isLocked = false;
        lakePortal.Use();

        lakePortal.lookText = holeLookAtMessage;
        lakePortal.useText = holeUseMessage;

        gameObject.SetActive( false );
        crackMesh.SetActive( false );
        holeMesh.SetActive( true );
    }
}

