using UnityEngine;
using System.Collections;

public class TreeInteraction : Interaction
{
    public Transform
        logTransform,
        snowPackTransfom;
    public string
        spotNonFreeText = "Something already in place";

    public bool GetIsClimblable()
    {
        return ( mIsLogInPlace || mIsSnowInPlace );
    }

    public override void Use ()
    {
        PlayerController
            player = GetGameManager().GetPlayer();

        if( player.handledPickableObject == null )
        {
            base.Use();
        }
        else if( player.handledPickableObject.name == "CompanionLog" )
        {
            if( !mIsSnowInPlace )
            {
                mLogObject = player.handledPickableObject;

                mLogObject.gameObject.transform.position = logTransform.position;
                mLogObject.gameObject.transform.rotation = logTransform.rotation;
                
                mLogObject.gameObject.SetActive( true );

                mLogObject.RegisterOnPickedFunction( OnLogPicked );
                
                player.transform.rotation = anchorPoint.rotation;
                player.ConsumeObject( false );
                
                mIsLogInPlace = true;
            }
            else
            {
                player.GetComponent<CharacterTextController>().Speak( spotNonFreeText );
            }

        }
        else if( player.handledPickableObject.name == "SnowPack" )
        {
            if( !mIsLogInPlace )
            {
                mSnowObject = player.handledPickableObject;
                
                mSnowObject.gameObject.transform.position = logTransform.position;
                mSnowObject.gameObject.transform.rotation = logTransform.rotation;
                
                mSnowObject.gameObject.SetActive( true );
                
                mSnowObject.RegisterOnPickedFunction( OnSnowPicked );
                
                player.transform.rotation = anchorPoint.rotation;
                player.ConsumeObject( false );
                
                mIsSnowInPlace = true;
            }
            else
            {
                player.GetComponent<CharacterTextController>().Speak( spotNonFreeText );
            }
        }
    }

    public override void Load( PersistentData persistent_data )
    {
        base.Load( persistent_data );

        PersistenceManager
            persistence_manager = GetGameManager().GetPersistenceManager();

        persistent_data.GetBoolData( "mIsSnowInPlace", out mIsSnowInPlace );
        persistent_data.GetBoolData( "mIsLogInPlace", out mIsLogInPlace );

        if( mIsSnowInPlace )
        {
            mSnowObject = persistence_manager.GetPersistentObject( "SnowPack" ).GetComponent<PickableObject>();
            mSnowObject.RegisterOnPickedFunction( OnSnowPicked );
        }

        if ( mIsLogInPlace )
        {
            mLogObject = persistence_manager.GetPersistentObject( "CompanionLog" ).GetComponent<PickableObject>();
            mLogObject.RegisterOnPickedFunction( OnLogPicked );
        }
    }

    public override void Save( PersistentData persistent_data )
    {
        base.Save( persistent_data );

        persistent_data.SetBoolData( "mIsSnowInPlace", mIsSnowInPlace );
        persistent_data.SetBoolData( "mIsLogInPlace", mIsLogInPlace );
    }

    private void OnLogPicked()
    {
        mIsLogInPlace = false;
    }
    
    private void OnSnowPicked()
    {
        mIsSnowInPlace = false;
    }

    private PickableObject
        mLogObject,
        mSnowObject;
    private bool
        mIsSnowInPlace = false,
        mIsLogInPlace = false;
}

