using UnityEngine;
using System.Collections;

public class MatchesInteraction : WetablePickableObject
{
    public Transform
        takeAnchor;
    public ShelfInteraction
        shelf;
    public string
        unreachableText = "It's way too high !";

    public override void Use ()
    {
        if( shelf.GetIsLogInPlace() )
        {
            isPickable = true;

            base.Use();
        }
        else
        {
            PlayerController
                player = GetGameManager().GetPlayer();

            isPickable = false;

            player.GetComponent<CharacterTextController>().Speak( unreachableText );
        }

    }

}

