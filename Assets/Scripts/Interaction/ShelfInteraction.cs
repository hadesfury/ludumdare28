using UnityEngine;
using System.Collections;

public class ShelfInteraction : Interaction
{
    public Transform
        logTransform;
    public PickableObject
        matchesBox;

    public bool GetIsLogInPlace ()
    {
        return mIsLogInPlace;
    }

    public override void Use ()
    {
        if( !mIsLogInPlace )
        {
            PlayerController
                player = GetGameManager().GetPlayer();

            if( player.handledPickableObject == null )
            {
                base.Use();
            }
            else if( player.handledPickableObject.name == "CompanionLog" )
            {
                PickableObject
                    handled_log = player.handledPickableObject;

                handled_log.gameObject.transform.position = logTransform.position;
                handled_log.gameObject.transform.rotation = logTransform.rotation;
                
                handled_log.gameObject.SetActive( true );
                
                player.handledPickableObject.RegisterOnPickedFunction( OnLogPicked );
                
                player.transform.rotation = anchorPoint.rotation;
                player.ConsumeObject( false );
                
                mIsLogInPlace = true;

            }
        }

    }

    private void OnLogPicked()
    {
        mIsLogInPlace = false;
        matchesBox.isPickable = false;
    }

    private bool
        mIsLogInPlace = false;
}

