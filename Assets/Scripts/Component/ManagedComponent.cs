using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ManagedComponent : MonoBehaviour
{
    public GameManager GetGameManager()
    {
		if( mGameManager == null )
        {
		    mGameManager = GameObject.Find( "GameManager" ).GetComponent<GameManager>();
        }
		
        return mGameManager;
    }
	
	private GameManager
		mGameManager = null;
}

