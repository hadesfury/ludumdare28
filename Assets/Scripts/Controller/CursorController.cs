﻿using UnityEngine;
using System.Collections;

public class CursorController : ManagedComponent
{
    public Texture2D
        defaultCursor,
        activeCursor;
	public float
		marginSize = 10;
    public Vector2
        cursorCenter;
    public dfSprite
        pickedObjectSprite;


    void Start()
    {
        Screen.showCursor = false;

//		guiText.anchor = TextAnchor.UpperCenter;
//		guiText.pixelOffset = new Vector2(0, -32);
//		mOriginalPixelOffset = new Vector2( guiText.pixelOffset.x, guiText.pixelOffset.y);
    }

    public void SetDefaultCursor ()
    {
        GetComponent<dfSprite>().SpriteName = defaultCursor.name;
    }

    public void SetActiveCursor ()
    {
        GetComponent<dfSprite>().SpriteName = activeCursor.name;
    }

    public void SetText( string text )
    {
		GetComponentInChildren<dfLabel>().Text = text;
    }

    void Update ()
    {        
        Vector3
            mouse_position = Input.mousePosition;

        //print ( mouse_position );

        GetComponent<dfSprite>().RelativePosition = new Vector3( mouse_position.x - cursorCenter.x, Screen.height - mouse_position.y - cursorCenter.y, 0 );


        //transform.position = new Vector3( mouse_position.x / Screen.width, mouse_position.y / Screen.height, transform.position.z );

//		// Update Text Position
//		//
//		Rect
//			text_screen_rect = guiText.GetScreenRect();
//
//		// Update x
//		//
//		float
//			// Offset between 0 and left of text
//			x_left_offset  = ( mouse_position.x - (text_screen_rect.width / 2)),
//			// Offset between screen widht and right of text
//			x_right_offset = ( ( mouse_position.x  + (text_screen_rect.width / 2) ) - Screen.width ); 
//
//		// If text is at left of screen limit ...
//		if (x_left_offset < marginSize)
//		{
//			// ... then adjust
//			guiText.pixelOffset = new Vector2(x_left_offset * (-1) + marginSize, guiText.pixelOffset.y);
//		}
//		// If text is at right of screen limit ...
//		else if (x_right_offset > marginSize * (-1))
//		{
//			// ... then adjust
//			guiText.pixelOffset = new Vector2(x_right_offset * (-1) - marginSize, guiText.pixelOffset.y);
//		}
//
//		// Update y
//		//
//		float
//			// Offset between 0 and bottom of text
//			y_bottom_offset  = ( mouse_position.y + mOriginalPixelOffset.y - text_screen_rect.height),
//			// Offset between screen height and top of text
//			y_top_offset  	 = (Screen.height - ( mouse_position.y + mOriginalPixelOffset.y));
//
//		// If text is at bottom of bottom most limit ...
//		if (y_bottom_offset < marginSize)
//		{
//			// ... then adjust
//			guiText.pixelOffset = new Vector2(guiText.pixelOffset.x, Mathf.Max(y_bottom_offset*(-1) + marginSize + mOriginalPixelOffset.y, mOriginalPixelOffset.y));
//		}
//		else if (y_top_offset < marginSize) 
//		{
//			guiText.pixelOffset = new Vector2(guiText.pixelOffset.x, y_top_offset + mOriginalPixelOffset.y - marginSize);
//		}
//
        UpdatePickedObject();
    }

    void UpdatePickedObject ()
    {
        PickableObject
            handle_object = GetGameManager().GetPlayer().handledPickableObject;

        if( handle_object != null )
        {            
            pickedObjectSprite.SpriteName = handle_object.inventoryIcon.name ;
            pickedObjectSprite.gameObject.SetActive( true );
        }
        else
        {
            pickedObjectSprite.gameObject.SetActive( false );
        }
    }

	private Vector2 
        mOriginalPixelOffset;
}
