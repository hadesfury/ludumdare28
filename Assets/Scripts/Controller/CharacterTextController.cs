﻿using UnityEngine;
using System.Collections;

public class CharacterTextController : ManagedComponent
{
    public dfLabel
        text;
    public float
        playerHeight = 1.5f,
        timingByCharacter = 0.06f;

    void Start()
    {
        text.gameObject.SetActive( false );
    }

    void Update ()
    {
        GameObject
            player = GetGameManager().GetPlayer().gameObject;
        Vector3
            text_screen_position = Camera.main.WorldToScreenPoint( player.transform.position + player.transform.up * playerHeight );

        text.RelativePosition = new Vector2( text_screen_position.x, text_screen_position.y );

    }

    public void Speak ( string message )
    {
        StartCoroutine( Speak ( message, timingByCharacter ) );
    }

    private IEnumerator Speak( string message, float timing_by_letter )
    {
        int
            writter_index = ++mWritterIndex;
        string[]
            sentence_table = message.Split( "/".ToCharArray() );

        
        text.gameObject.SetActive( true );

        foreach( string current_sentence in sentence_table )
        {
            if( writter_index == mWritterIndex )
            {
                text.Text = current_sentence;

                yield return new WaitForSeconds( current_sentence.Length * timing_by_letter );
            }
            else
            {
                break;
            }
        }

        if( writter_index == mWritterIndex )
        {
            text.Text = "";
        }

        
        text.gameObject.SetActive( false );
    }

    private int
        mWritterIndex = 0;
}
