﻿using UnityEngine;
using System.Collections;

public class PlayerController : ManagedComponent
{
	public PickableObject
		handledPickableObject;
	public AudioClip
		footStepClip,
		useClip;

	// Use this for initialization
	void Start ()
	{
//		mGroundPlane = new Plane( Vector3.zero, Vector3.up );
        mNavMeshAgent = GetComponent<NavMeshAgent>();

		mStateMachine = new StateMachine<PlayerController, string>( this );

		mStateMachine.RegisterState( new IdleState() );		
        mStateMachine.RegisterState( new WalkState() );
        mStateMachine.RegisterState( new UseState() );

		mStateMachine.SetCurrentState( typeof( IdleState ) );
	}


	public void ChangeState( System.Type state_type )
	{
		mStateMachine.ChangeState( state_type );
	}

	// Update is called once per frame
	void Update ()
	{
		mStateMachine.Update( Time.deltaTime );
	}

    public void OnLeftClick()
    {
        Interaction
            interaction = GetGameManager().GetInteractionObject();

        //print( interactive_object );

        if( interaction != null )
        {
            mCurrentTarget = interaction;

            mNavMeshAgent.SetDestination( mCurrentTarget.anchorPoint.position ); 
			
			ChangeState( typeof( WalkState ) );

            StartCoroutine( UseTarget() );
                // when destination is reached use object
        }
        else
        {
            StopMovement();
             MoveToGround();
        }
	}

    private IEnumerator UseTarget()
    {
        while( mCurrentTarget != null )
        {
            if( ( mNavMeshAgent.hasPath || !mNavMeshAgent.pathPending ) && ( mNavMeshAgent.pathStatus == NavMeshPathStatus.PathComplete ) && (  mNavMeshAgent.remainingDistance <= mNavMeshAgent.stoppingDistance ) )
            {
                ChangeState( typeof( UseState ) );
                mCurrentTarget.Use();
                mCurrentTarget = null;
            }

            yield return null;
        }
    }
        
    public void ConsumeObject ( bool is_destroyed )
    {
        PersistenceManager
            persistence_manager = GetGameManager().GetPersistenceManager();

        persistence_manager.RemoveFromPlayerInventory( handledPickableObject );

        if( !is_destroyed )
        {
            persistence_manager.AddPersistentObjectToScene( handledPickableObject.GetComponent<PersistentObject>(), Application.loadedLevelName );
        }

        handledPickableObject = null;
    }

	public void OnRightClick()
    {
		if( handledPickableObject != null )
        {
            PutBackHandledObjectInInventory ();
		}
		else
		{
            Interaction
                interaction = GetGameManager().GetInteractionObject();

            if( interaction != null )
            {
                StopMovement();

                interaction.Look();
            }
		}
	}

    public void PutBackHandledObjectInInventory ()
    {
        GetGameManager().GetInventoryManager().ShowInInventory( handledPickableObject );
        handledPickableObject = null;
    }

    void StopMovement()
    {
        mCurrentTarget = null;
        mNavMeshAgent.Stop();
        ChangeState( typeof( IdleState ) );

    }
    

    
    void MoveToGround()
    {
        Ray
            mouse_ray = Camera.main.ScreenPointToRay( Input.mousePosition );
        RaycastHit
            raycast_hit;
        
        if( Physics.Raycast( mouse_ray, out raycast_hit ) )
        {
            Vector3
                raycast_hit_point = raycast_hit.point;
                        
            mNavMeshAgent.SetDestination( raycast_hit_point ); 
			
			ChangeState( typeof( WalkState ) );
        }
    }

    public void SetCurrentTarget( Interaction current_target )
    {
        mCurrentTarget = current_target;
    }

//	private Plane
//		mGroundPlane;
    private Interaction
        mCurrentTarget;
    private NavMeshAgent
        mNavMeshAgent;
	private StateMachine<PlayerController, string>
		mStateMachine;
}
