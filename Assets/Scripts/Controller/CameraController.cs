﻿using UnityEngine;
using System.Collections;

public class CameraController : ManagedComponent
{
    public float
        anglePanTolerance = 20,
        anglePanMax = 90;

    void Start()
    {
        mInitialOrientation = transform.rotation;
    }

    void Update ()
    {
		transform.rotation = mInitialOrientation;
		
		float
			half_screen_width = Screen.width / 2;
        Transform
            player = GetGameManager().GetPlayer().transform;
        Vector3
            player_screen_position = camera.WorldToScreenPoint( player.position );
        float 
            angle_to_player = Vector3.Angle( transform.forward, ( player.position - transform.position ).normalized );

        //print( "Angle to player : " + angle_to_player );
        if( angle_to_player > anglePanTolerance )
        {
            float
                angle_sign = Mathf.Sign( player_screen_position.x - half_screen_width ),
                angle_offset = Mathf.Min( angle_to_player, anglePanMax ) - anglePanTolerance;

            transform.Rotate( transform.up, angle_sign * angle_offset );
        }

    }

    private Quaternion
        mInitialOrientation;
}
