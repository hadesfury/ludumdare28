using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Sentence
{
    public float
        timing;
    public string
        text;
}

