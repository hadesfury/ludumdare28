using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ObjectDialog
{
    public List<ObjectDescription>
        interactionTextTable;
    public ObjectDescription
        defaultInteractionText;

    public List<Sentence> GetText ( string object_name )
    {
        foreach( ObjectDescription current_description in interactionTextTable )
        {
            if( current_description.objectName == object_name )
            {
                return current_description.sentenceTable;
            }
        }

        return defaultInteractionText.sentenceTable;
    }
}

