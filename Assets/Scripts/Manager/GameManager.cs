﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ RequireComponent( typeof( InputManager ) ) ]
[ RequireComponent( typeof( PersistenceManager ) ) ]
public class GameManager : MonoBehaviour 
{
    public List<Texture2D>
        introCutScene,
        gameOverWetLogCutScene,
        gameOverWetMatchesCutScene,
        gameOverCutScene;
    public float
        gameOverCutSceneDuration = 10.0f;

    public void Register ( GuiManager gui_manager )
    {
        mGuiManager = gui_manager;
    }

    public void ChangeScene ( string scene_name )
    {
        GetPersistenceManager().Save( "AutoSave" );
        StartCoroutine( GetPersistenceManager().Load( "AutoSave", scene_name ) );
    }

    public PlayerController GetPlayer()
    {
        PlayerController
            player_controller = GameObject.Find( "Player" ).GetComponent<PlayerController>();

        return player_controller;
    }

    public CutScenePlayer GetCutScenePlayer()
    {
        CutScenePlayer
            current_cut_scene_player = GameObject.FindObjectOfType<CutScenePlayer>();

        return current_cut_scene_player;
    }

    public void PlayIntroCutScene ()
    {
        GetCutScenePlayer().PlayCutScene( introCutScene );
    }
    
    public void PlayGameOverWetLogCutScene ()
    {
        GetCutScenePlayer().PlayCutScene( gameOverWetLogCutScene, gameOverCutSceneDuration );
    }
    
    public void PlayGameOverWetMatchesCutScene ()
    {
        GetCutScenePlayer().PlayCutScene( gameOverWetMatchesCutScene, gameOverCutSceneDuration );
    }

    public void PlayGameOverCutScene ()
    {
        GetCutScenePlayer().PlayCutScene( gameOverCutScene, gameOverCutSceneDuration );
    }
    
    public GuiManager GetGuiManager()
    {
        return mGuiManager;
    }

    public InputManager GetInputManager ()
    {
        return GetComponent<InputManager>();
    }

    public PersistenceManager GetPersistenceManager ()
    {
        return GetComponent<PersistenceManager>();
    }
    
    public InventoryManager GetInventoryManager()
    {
        InventoryManager
            inventory_gui = GameObject.Find( "InventoryManager" ).GetComponent<InventoryManager>();

        return inventory_gui;
    }

	void Start ()
	{
        GameManager[] 
            game_manage_table = GameObject.FindObjectsOfType<GameManager>();

        foreach( GameManager current_game_manager in game_manage_table )
        {
            if( current_game_manager != this )
            {
                GameObject.Destroy( gameObject );
            }
        }

        DontDestroyOnLoad( this );

        mGameStateMachine = new StateMachine<GameManager, string>( this );

        mGameStateMachine.RegisterState( new GameStateMenu() );
        mGameStateMachine.RegisterState( new GameStatePlay() );
        mGameStateMachine.RegisterState( new GameStateGameOver() );
        mGameStateMachine.RegisterState( new GameStateWinOver() );

        mGameStateMachine.SetCurrentState( typeof( GameStateMenu ) );

	}

    public void ChangeGameState( System.Type game_state_type )
    {
        mGameStateMachine.ChangeState( game_state_type );
    }

	void OnGUI()
	{
		if (exitMenuDisplay)
		{
			if ( GUI.Button(new Rect(100,100,200,200),"EXIT") )
			{
				Application.Quit();
			}
			if ( GUI.Button(new Rect(400,100,200,200),"CANCEL"))
			{
				exitMenuDisplay = false;
			}
		}
	}

    void Update()
    {
        mGameStateMachine.Update( Time.deltaTime );
#if UNITY_ANDROID || UNITY_STANDALONE
		if ( Input.GetKeyUp(KeyCode.Escape))
		{
			if (exitMenuDisplay)
			{
				exitMenuDisplay = false;
			}
			else
			{
				exitMenuDisplay = true;
			}
			print (exitMenuDisplay);
		}
#endif
    }

    public Interaction GetInteractionObject()
    {
        Ray
            mouse_ray = Camera.main.ScreenPointToRay( Input.mousePosition );
        RaycastHit
            raycast_hit;
        Interaction
            interaction_object = null;
        
        if( Physics.Raycast( mouse_ray, out raycast_hit ) )
        {
            interaction_object = raycast_hit.collider.transform.GetComponent<Interaction>();
        }
        
        return interaction_object;
    }

    private StateMachine<GameManager,string>
        mGameStateMachine;
	private bool 
        exitMenuDisplay = false;
    private GuiManager
        mGuiManager;
}
