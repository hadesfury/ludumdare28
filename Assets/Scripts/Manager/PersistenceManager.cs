﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class PersistenceManager : ManagedComponent
{
    public string
        savePath;
    public bool
        isLoaded = false;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad( this );
    }

    void OnGUI()
    {
        if ( GUI.Button( new Rect( 0, 0, 50, 50 ), "Save" ) )
        {
            Save( "ManualSave" );
        }

        if ( GUI.Button( new Rect( Screen.width - 50, 0, 50, 50 ), "Load" ) )
        {
            StartCoroutine( Load( "ManualSave", "" ) );
        }
    }

    public void Register( PersistentObject persistent_object )
    {
        if ( mPersistentObjectTable.ContainsKey( persistent_object.objectName ) )
        {
            Destroy( persistent_object.gameObject );
        }
        else
        {
            mPersistentObjectTable.Add( persistent_object.objectName, persistent_object );
        }

        persistent_object.gameObject.SetActive( !isLoaded );
    }

    public void Unregister( PersistentObject persistent_object )
    {
        if ( mPersistentObjectTable.ContainsKey( persistent_object.objectName ) )
        {
            mPersistentObjectTable.Remove( persistent_object.objectName );
        }
    }

    public void AddToPlayerInventory( PersistentObject persistent_object )
    {
        mPersistentGeneral.player.inventoryDataTable.Add( persistent_object.objectName, persistent_object.GetPersistentData() );
    }

    public void RemoveFromPlayerInventory( PersistentObject persistent_object )
    {
        if ( mPersistentGeneral.player.inventoryDataTable.ContainsKey( persistent_object.objectName ) )
        {
            mPersistentGeneral.player.inventoryDataTable.Remove( persistent_object.objectName );
        }
        else
        {
            Debug.LogError( persistent_object.objectName + " does not exist in inventory" );
        }
    }

    public void AddPersistentObjectToScene( PersistentObject persistent_object, string scene_name )
    {
        if ( !mPersistentGeneral.sceneTable.ContainsKey( scene_name ) )
        {
            mPersistentGeneral.sceneTable.Add( scene_name, new PersistentScene() );
        }

        if ( !mPersistentGeneral.sceneTable[ scene_name ].sceneDataTable.ContainsKey( persistent_object.objectName ) )
        {
            mPersistentGeneral.sceneTable[ scene_name ].sceneDataTable.Add( persistent_object.objectName, persistent_object.GetPersistentData() );
        }
        else
        {
            Debug.LogError( persistent_object.objectName + " already exists in " + scene_name );
        }
    }

    public void RemovePersistentObjectFromScene( PersistentObject persistent_object, string scene_name )
    {
        if ( !mPersistentGeneral.sceneTable.ContainsKey( scene_name ) )
        {
            Debug.LogError( scene_name + " does not exist" );
        }

        if ( mPersistentGeneral.sceneTable[ scene_name ].sceneDataTable.ContainsKey( persistent_object.objectName ) )
        {
            mPersistentGeneral.sceneTable[ scene_name ].sceneDataTable.Remove( persistent_object.objectName );
        }
        else
        {
            Debug.LogError( persistent_object.objectName + " does not exist in " + scene_name );
        }
    }

    public void AddToPlayerInventory( PickableObject pickable_object )
    {
        PersistentObject
            persistent_object = pickable_object.GetComponent<PersistentObject>();

        RemovePersistentObjectFromScene( persistent_object, Application.loadedLevelName );
        AddToPlayerInventory( persistent_object );
    }

    public void RemoveFromPlayerInventory( PickableObject pickable_object )
    {
        PersistentObject
            persistent_object = pickable_object.GetComponent<PersistentObject>();

        RemoveFromPlayerInventory( persistent_object );
        //AddPersistentObjectToScene( persistent_object, Application.loadedLevelName );
    }

    public IEnumerator Load( string save_name, string scene_name )
    {
        string
            current_scene_name,
            json_save = PlayerPrefs.GetString( save_name );
        PersistentGeneral
            save_data = fastJSON.JSON.Instance.ToObject<PersistentGeneral>( json_save );

        if ( scene_name.Length > 0 )
        {
            current_scene_name = scene_name;
        }
        else
        {
            current_scene_name = save_data.currentSceneName;
        }

        if ( save_data.sceneTable.ContainsKey( current_scene_name ) )
        {
            yield return StartCoroutine( LoadScene( current_scene_name, save_data ) );
        }

        yield return StartCoroutine( save_data.player.Load( GetGameManager().GetPlayer() ) );

    }
    
    IEnumerator LoadScene( string current_scene_name, PersistentGeneral save_data )
    {
        List<PersistentObject>
            loaded_object_table = new List<PersistentObject>();

        isLoaded = true;

        Application.LoadLevel( current_scene_name );

        while ( Application.isLoadingLevel )
        {
            yield return new WaitForFixedUpdate();
        }
        
        foreach ( PersistentData current_data in save_data.sceneTable[ current_scene_name ].sceneDataTable.Values )
        {
            loaded_object_table.Add( LoadPersistentObject( current_data ) );
        }

        mPersistentObjectTable.Clear();

        foreach ( PersistentObject current_object in loaded_object_table )
        {
            mPersistentObjectTable.Add( current_object.objectName, current_object );
        }
    }

    public PersistentObject GetPersistentObject( string name )
    {
        return mPersistentObjectTable[ name ];
    }

    public PersistentObject LoadPersistentObject( PersistentData object_data )
    {
        PersistentObject
            persistent_object = null;

        if ( mPersistentObjectTable.ContainsKey( object_data.name ) )
        {
            persistent_object = mPersistentObjectTable[ object_data.name ];
            persistent_object.Load( object_data );
        }
        else
        {
            if ( object_data.prefabName.Length > 0 )
            {
                PrefabManager
                    prefab_manager = GetGameManager().GetComponent<PrefabManager>();

                foreach ( GameObject current_prefab in prefab_manager.prefabTable )
                {
                    if ( current_prefab.name == object_data.prefabName )
                    {
                        GameObject
                            game_object = ( GameObject ) GameObject.Instantiate( current_prefab );
                        
                        persistent_object = game_object.GetComponent<PersistentObject>();

                        persistent_object.Load( object_data );

                        mPersistentObjectTable.Add( object_data.name, persistent_object );

                        break;
                    }
                }
            }
            else
            {
                Debug.LogError( "Missing object : " + object_data.name );
            }
        }

        return persistent_object;
    }

    private string SaveToText()
    {
        fastJSON.JSONParameters
            json_parameter = new fastJSON.JSONParameters() { EnableAnonymousTypes = true, ShowReadOnlyProperties = true };
        string
            saved_data,
            current_scene_name = Application.loadedLevelName;

        mPersistentGeneral.currentSceneName = current_scene_name;
        
        mPersistentGeneral.player.Save( GetGameManager().GetPlayer() );

        foreach ( PersistentScene current_persistent_scene in mPersistentGeneral.sceneTable.Values )
        {
            foreach ( string current_scene_object_name in current_persistent_scene.sceneDataTable.Keys )
            {
                if( mPersistentObjectTable.ContainsKey( current_scene_object_name ) )
                {
                    mPersistentObjectTable[ current_scene_object_name ].Save();
                }
            }
        }

        saved_data = fastJSON.JSON.Instance.Beautify( fastJSON.JSON.Instance.ToJSON( mPersistentGeneral, json_parameter ) );

        return saved_data;
    }
    
    public void Save( string save_name )
    {
        string
            saved_data = SaveToText();

        PlayerPrefs.SetString( save_name, saved_data );

        #if !UNITY_WEBPLAYER
                SaveOnDisk( saved_data );
        #endif

        //Load( saved_data );
    }

    private void SaveOnDisk( string saved_data )
    {
        #if !UNITY_WEBPLAYER
                string
                    file_name = "game",
                    file_extension = ".sav",
                    current_file_name,
                    final_save_path = Application.dataPath + "/../" + savePath; // :TODO: check on linux/mac

                current_file_name = final_save_path
                    + file_name
                        + String.Format(
                            "-{00:0000}-{01:00}-{02:00}.{03:00}-{04:00}-{05:00}",
                            System.DateTime.Now.Year,
                            System.DateTime.Now.Month,
                            System.DateTime.Now.Day,
                            System.DateTime.Now.Hour,
                            System.DateTime.Now.Minute,
                            System.DateTime.Now.Second )
                        + file_extension;

                Directory.CreateDirectory( final_save_path );

                if ( !File.Exists( current_file_name ) )
                {
                    StreamWriter
                        stream = File.CreateText( current_file_name );

                    stream.WriteLine( saved_data );

                    stream.Close();
                }
                else
                {
                    Debug.LogError( "Clean save file" );
                }
        #endif
    }

    private Dictionary<string, PersistentObject>
        mPersistentObjectTable = new Dictionary<string, PersistentObject>();
    private PersistentGeneral
        mPersistentGeneral = new PersistentGeneral();
}
