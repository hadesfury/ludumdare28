using UnityEngine;
using System.Collections;

public class InputManager : ManagedComponent
{
    public bool
        areInputsLocked = false,
        hasInputBeenUsed = false;

    void Update ()
    {        
        if( !hasInputBeenUsed && !areInputsLocked )
        {
            if( Input.GetMouseButtonUp( 0 ) )
            {
                GetGameManager().GetPlayer().OnLeftClick();
            }
            
            if( Input.GetMouseButtonUp( 1 ) || Input.touchCount > 1 )
            {
                GetGameManager().GetPlayer().OnRightClick();
            }

            if( Input.GetKeyUp( KeyCode.I ) )
            {
                GetGameManager().GetInventoryManager().OpenCloseInventory();
            }
        }
    }
        
    void FixedUpdate()
    {
        hasInputBeenUsed = false;
    }

}

