using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryManager : ManagedComponent
{        
    public void AddToInventory ( PickableObject pickable_object )
    {
        PersistenceManager
            persistence_manager = GetGameManager().GetPersistenceManager();

        mInventoryTable.Add( pickable_object );
        persistence_manager.AddToPlayerInventory( pickable_object );        
   
        pickable_object.gameObject.SetActive( false );   
    }
     
    public void OnClick( dfControl control, dfMouseEventArgs mouseEvent )
    {
        InputManager
            input_manager = GetGameManager().GetInputManager();

        if( !input_manager.hasInputBeenUsed )
        {
            OpenCloseInventory();
        }

        input_manager.hasInputBeenUsed = true;
    }          

    public void PickItem( PickableObject pickable_object )
    {
        PlayerController
            player = GetGameManager().GetPlayer();

        if( player.handledPickableObject != null )
        {
            player.PutBackHandledObjectInInventory ();
        }

        player.handledPickableObject = pickable_object;
        HideFromInventory( pickable_object ); // HACK not really elegant
    }

    public void ShowInInventory( PickableObject pickable_object )
    {
        if( !mInventoryTable.Contains( pickable_object ) )
        {
            mInventoryTable.Add( pickable_object );
        }
    }

    private void HideFromInventory( PickableObject pickable_object )
    {
        if ( mInventoryTable.Contains( pickable_object ) )
        {
            mInventoryTable.Remove( pickable_object );
        }
    }

	public void OpenCloseInventory()
	{		
        mIsInventoryOpen = !mIsInventoryOpen;
	}

    public IEnumerable<PickableObject> GetInventoryTable()
    {
        return mInventoryTable;
    }

    public bool IsInventoryOpen ()
    {
        return mIsInventoryOpen;
    }

    private List<PickableObject>
        mInventoryTable = new List<PickableObject>();
    private bool
        mIsInventoryOpen = false;
}
