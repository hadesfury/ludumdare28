using UnityEngine;
using System.Collections;

public class GuiManager : ManagedComponent
{
    public dfPanel
        inventoryPanel;

    void Start()
    {
        GetGameManager().Register( this );
        
        mInventoryButtonTable = inventoryPanel.GetComponentsInChildren<dfButton>();
    }

    void Update()
    {
        UpdateCursor();
        UpdateInventoryGui();
    }

    public void UpdateCursor()
    {
        Interaction
            interaction = GetGameManager().GetInteractionObject();
        CursorController
            cursor = GetCursor();
        
        if( interaction != null )
        {
            interaction.DisplayCursorText();
            cursor.SetActiveCursor();
        }
        else if( cursor != null )
        {
            cursor.SetDefaultCursor();
            cursor.SetText( "" );
        }
    }

    public CursorController GetCursor()
    {
        GameObject
            cursor = GameObject.Find( "Cursor" );
        
        if( cursor != null )
        {
            return cursor.GetComponent<CursorController>();
        }
        
        return null;
    }

    public void UpdateInventoryGui()
    {        
        int
            object_index = 0;
        InventoryManager
            inventory_manager = GetGameManager().GetInventoryManager();
        
        inventoryPanel.gameObject.SetActive( inventory_manager.IsInventoryOpen() );
        
        foreach( dfButton current_slot in mInventoryButtonTable )
        {
            current_slot.gameObject.SetActive( false );
            
            current_slot.GetComponent<dfEventBinding>().enabled = false;
        }
        
        foreach( PickableObject current_object in inventory_manager.GetInventoryTable() )
        {
            if( mInventoryButtonTable.Length > object_index )
            {
                mInventoryButtonTable[ object_index ].gameObject.SetActive( true );
                
                dfSprite
                    sprite = mInventoryButtonTable[ object_index ].GetComponentInChildren<dfSprite>();
                
                if( sprite != null )
                {
                    sprite.SpriteName  = current_object.inventoryIcon.name;
                }
                
                mInventoryButtonTable[ object_index ].GetComponent<InventorySlot>().RegisterClickPickableEvent( inventory_manager.PickItem, current_object );
                //guiButtonTable[ object_index ].GetComponent<dfEventBinding>().DataSource.( PickItem, current_object ); 
                
            }
            else
            {
                Debug.LogError( "Inventory Gui too small" );
            }
            
            ++object_index;
        }

    }

    private dfButton[]
        mInventoryButtonTable;
}

