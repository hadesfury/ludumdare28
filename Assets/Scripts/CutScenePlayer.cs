using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CutScenePlayer : ManagedComponent
{
    public float
        defaultImageDuration = 5;
    public GameObject
        cutScenePlayerPrefab;

    void Start ()
    {
        CutScenePlayer[] 
            player_table = GameObject.FindObjectsOfType<CutScenePlayer>();
        
        foreach( CutScenePlayer current_player in player_table )
        {
            if( current_player != this )
            {
                GameObject.Destroy( gameObject );
            }
        }
        
        DontDestroyOnLoad( this );

        UpdateUi ();

        if( mBackground != null )
        {
            mBackground.enabled = false;
        }
    }
    
    public void PlayCutScene( List<Texture2D> cut_scene_table, float duration )
    {
        if( !mIsPlayingCutScene )
        {
            StartCoroutine( PlayCutSceneCoroutine( cut_scene_table, duration ) );
        }
    }

    public void PlayCutScene( List<Texture2D> cut_scene_table )
    {
        PlayCutScene( cut_scene_table, defaultImageDuration );
    }
                       
    public IEnumerator PlayCutSceneCoroutine( List<Texture2D> cut_scene_table, float duration )
    {
        InputManager
            input_manager = GetGameManager().GetInputManager();

        input_manager.areInputsLocked = true;

        mIsPlayingCutScene = true;
        
        UpdateUi ();

        mBackground.enabled = true;
        
        foreach( Texture2D texture in cut_scene_table )
        {
            mCurrentTexture = texture;

            FormatScreenTexture();

            yield return new WaitForSeconds( duration );

        }

        mBackground.enabled = false;

        mIsPlayingCutScene = false;

        input_manager.areInputsLocked = false;
        
//        if ( mDeactivatedCursor != null )
//        {
//            mDeactivatedCursor.gameObject.SetActive( true );
//        }
    }

    private void FormatScreenTexture()
    {
        int
            texture_width = mCurrentTexture.width,
            texture_height = mCurrentTexture.height;
        
        float 
            //texture_aspect_ratio = texture_width / texture_height,
            texture_diplay_ratio = 1;
        
        
        if( Screen.width < texture_width )
        {
            texture_diplay_ratio = Screen.width / ( float ) texture_width;                
        }
        
        
        if( Screen.height < texture_height )
        {
            float
                height_texture_ratio = Screen.width / ( float ) texture_width;
            
            if( height_texture_ratio < texture_diplay_ratio )
            {
                texture_diplay_ratio = height_texture_ratio;
            }
        }
        
        mCutSceneSprite.SpriteName = mCurrentTexture.name;

        mCutSceneSprite.Size = new Vector2( mCurrentTexture.width, mCurrentTexture.height );

        mCutSceneSprite.Size *= texture_diplay_ratio;
    }

    void Update()
    {
        if( mIsPlayingCutScene )
        {
            UpdateUi ();

            FormatScreenTexture();

            FitToScreen();
            
            //backgroundPlane.transform.position = Camera.main.transform.position + Camera.main.transform.forward;

//            if( mDeactivatedCursor == null )
//            {
//                mDeactivatedCursor = GetGameManager().GetGuiManager().GetCursor();
//
//                if ( mDeactivatedCursor != null )
//                {
//                    mDeactivatedCursor.gameObject.SetActive( false );
//                }
//            }
        }
    }

    void FitToScreen ()
    {
        mBackground.Size = new Vector2( Screen.width, Screen.height );
    }

    void UpdateUi ()
    {
        CutScenePlayerUi    
            current_cut_scene_player = GameObject.FindObjectOfType<CutScenePlayerUi> ();

        if ( current_cut_scene_player == null ) 
        {
            current_cut_scene_player = ( ( GameObject ) GameObject.Instantiate ( cutScenePlayerPrefab ) ).GetComponent<CutScenePlayerUi>();

//            dfControl
//                control = current_cut_scene_player.gameObject.GetComponent<dfControl>();
//            //current_cut_scene_player.transform.parent = 
//            GameObject.FindObjectOfType<dfGUIManager>().AddControl( control );
//
//            control.BringToFront();
        }

        mCutSceneSprite = current_cut_scene_player.cutSceneSprite;
        if( mCurrentTexture != null )
        {
            mCutSceneSprite.SpriteName = mCurrentTexture.name;
        }
        mBackground = current_cut_scene_player.background;
        mBackground.enabled = true;
    }

    private bool
        mIsPlayingCutScene = false;
//    private CursorController
//        mDeactivatedCursor;
    private dfSprite
        mCutSceneSprite;
    private dfPanel
        mBackground;
    private Texture
        mCurrentTexture;
}

