﻿using UnityEngine;
using System.Collections;

public class LightFlicker : MonoBehaviour
{
	public float
		power = 1,
		speed = 1;
	private float baseIntensity;

	// Use this for initialization
	void Start ()
	{
		baseIntensity = light.intensity;
	}
	
	// Update is called once per frame
	void Update ()
	{
		light.intensity = baseIntensity + power * Mathf.Sin (Time.realtimeSinceStartup * speed);
	}
}
